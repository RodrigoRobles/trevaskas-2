#!/bin/sh -x
nasm -felf64 -Wall gamelib.asm -g -o gamelib.o
nasm -felf64 -Wall trevaskas2.asm -g -o trevaskas2.o
gcc -no-pie -march=native -m64 -O3 -Wall -Wextra trevaskas2.o gamelib.o -lGL -lopenal -lalut -lglut -g -o trevaskas2
