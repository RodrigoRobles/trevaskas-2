# Trevaskas 2

TREVASKAS II is a remake of Trevaskas, Indie game from 1995 inspired by Attack of the Timelord. 

This game was made in 100% x86_64 Linux Assembly as an experiment to analyze the viability of making large programs with modern Assembly.

Screenshot of TREVASKAS II:
![screenshot](https://drive.google.com/uc?id=1VKTaW_niQpLXNNIRDV-jNkaw5p4oyUmT&export=download)

You can find the original Trevaskas from 1995 here: https://drive.google.com/uc?id=1inqi-xs6zOoTKurBRdUOFCD6QancgBIb&export=download
(It’s a MSDOS app and must be run under an emulator like DOSBOX).

Screenshot of 1995’s TREVASKAS:

![screenshot](https://drive.google.com/uc?id=1hqfrG1tQjro5tR5ekiXBTC8ag-JTUMVS&export=download)

Attack of the Timelord is a Odissey2 game from 1982. You can get more info about this game here: https://www.the-nextlevel.com/odyssey2/reviews/attack-of-the-timelord-6

Screenshot of 1982’s Attack of the Timelord:

![screenshot](https://drive.google.com/uc?id=1ihLoYKW1mPx_gvEUgW2JZB8963_4oLo4&export=download)

## Features

* Opengl graphics;
* Fullscreen or windowed mode;
* Openal sound effects;
* Joystick input;
* FPS Display;
* FPS Delimiter.

## Program switches

| Switch            | Description                                   |
| -------------     | -------------                                 |
| –windowed         | Enable windowed mode (Default is fullscreen)  |
| –fps              | Enable show FPS                               |
| –fpsdelimiter     | Enable FPS delimiter at 100 FPS               |

## System requirements

Processor with RDRAND support (Intel Core i3 or AMD Ryzen)

