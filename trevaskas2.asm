;	trevaskas2.asm
;	Assembly Linux x86_64 Game using opengl and openal
;	To assemble:
;		nasm -felf64 -Wall trevaskas2.asm -g -o trevaskas2.o
;	To link:
;		gcc -no-pie -march=native -m64 -O3 -Wall -Wextra trevaskas2.o gamelib.o -lGL -lopenal -lalut -lglut -g -o trevaskas2
;	To run:
;		./trevaskas2
;-------------------------------------------------------------------------------------

default rel
global main

;gl functions
extern glClearColor
extern glFlush
extern glEnable
extern glDisable
extern glShadeModel
extern glClearDepth
extern glDepthFunc
extern glHint
extern glBlendFunc
extern glRasterPos2i
extern glWindowPos2i
extern glPushMatrix
extern glPopMatrix
extern glScalef
extern glTexEnvf
extern glTranslatef

;glut functions
extern glutInit
extern glutInitDisplayMode
extern glutInitWindowSize
extern glutInitWindowPosition
extern glutCreateWindow
extern glutDisplayFunc
extern glutMainLoop
extern glutSpecialFunc
extern glutSpecialUpFunc
extern glutPostRedisplay
extern glutIdleFunc
extern glutLeaveMainLoop
extern glutSetOption
extern glutKeyboardFunc
extern glutKeyboardUpFunc
extern glutBitmapString
extern glutStrokeString
extern glutBitmapHelvetica18
extern glutStrokeRoman
extern glutGameModeString
extern glutEnterGameMode
extern glutSetCursor
extern glutIgnoreKeyRepeat

;al functions
extern alutInit
extern alutCreateBufferFromFile
extern alGenSources
extern alSourcei
extern alSourcePlay
extern alSourceStop
extern alutExit
extern alDeleteSources
extern alDeleteBuffers

;gamelib functions
extern loadtex
extern inttostr
extern drawspr
extern drawrotatedspr
extern draw_text_pair
extern draw_text
extern printinteger
extern clrscr
extern print
extern mystrcmp
extern strzero
extern strcat

struc ship
	.x resq 1
	.y resq 1
	.prior resq 1
	.next resq 1
	.destroyed resq 1
	.goalx resq 1
	.goaly resq 1
	.timedestroyed resq 1
endstruc

struc enemy
	.x resq 1
	.y resq 1
	.kind resq 1
	.destroyed resq 1
	.timedestroyed resq 1
	.xorigin resq 1
endstruc

struc jsevent
	.time resd 1
	.value resw 1
	.etype resb 1
	.number resb 1
endstruc

%macro check_stack_alignment 0
		push rcx
		push r11
		mov [temp], rax
		mov rax, rsp
		and rax, 0b0000000000000000000000000000000000000000000000000000000000001111
		jz %%aligned
		mov rax, 1; sys_write
		mov rdi, 1
		mov rsi, STACK_ERROR_MESSAGE
		mov rdx, STACK_ERROR_MESSAGE_LEN
		syscall
		mov rax, 60; sys_exit
		mov rdi, 0
		syscall
%%aligned:	pop r11
		pop rcx
		mov rax, [temp]
%endmacro

%macro debug_number 1
		push rax
		push rdi
		push rsi
		push rcx
		push r11
		push rdx
		push r9
		mov rdi, %1
		mov rsi, sharpstr
		call printinteger	
		pop r9
		pop rdx
		pop r11
		pop rcx
		pop rsi
		pop rdi
		pop rax
%endmacro

;----------------------------------------------------
section .data

; check_stack_alignment macro data
STACK_ERROR_MESSAGE 			db 	"Stack misaligned!", 10
STACK_ERROR_MESSAGE_LEN 		equ 	$ - STACK_ERROR_MESSAGE
temp					dq	0

sharpstr				db	"# ", 0

; Strings
GAME_OVER_STRING 			db 	"GAME OVER", 0
SCORE_LABELSTR 				db 	"SCORE: ", 0
STAGE_LABELSTR 				db 	"STAGE: ", 0
HIGH_SCORE_STR				db	"HIGH SCORE: ", 0
FPS_LABELSTR 				db 	"FPS: ", 0
WINTITLE 				db 	"TREVASKAS II", 0
PRESS_ANY_KEY_STR			db	"PRESS ANY KEY", 0
QUESTION_MARK_STR			db	"????????", 0
RECORDIST_STR				db	"????????", 0
GAME_MODE_STRING			db	":32", 0
TREVASKASII_COPYRIGHT_MESSAGE		db	"TREVASKAS II Copyright TEZON Software 2022", 10
TREVASKASII_COPYRIGHT_MESSAGE_LEN	equ	$ - TREVASKASII_COPYRIGHT_MESSAGE
VERSION_MESSAGE				db	"Version 0.1 dd/mm/yyyy", 10
VERSION_MESSAGE_LEN			equ	$ - VERSION_MESSAGE
WINDOWED_STR				db	"--windowed", 0
FPS_STR					db	"--fps", 0
FPS_DELIMITER_STR			db	"--fpsdelimiter", 0
HELP_STR				db	"--help", 0
INVALID_ARG_STR				db	"Invalid Argument!", 0
TRY_HELP_STR				db	`Try \"trevaskas2 --help\" for more info.`, 0
POSSIBLE_ARGUMENTS_STR			db	"Possible arguments:", 0
NEW_LINE_STR				db	0
ARGUMENT_WINDOWED_STR			db	"  --windowed		Windowed mode", 0
ARGUMENT_FPS_STR			db	"  --fps			Enable FPS display", 0
ARGUMENT_FPS_DELIMITER_STR		db	"  --fpsdelimiter	Enable FPS delimiter", 0
ARGUMENT_HELP_STR			db	"  --help		Show this message", 0

; Bitmap files
BACKGROUND_PATH 			db 	"background.bmp", 0    
CANNON_PATH 				db 	"cannon.bmp", 0   
SHOT_PATH 				db 	"shot.bmp", 0   
SHIP_PATH 				db 	"ship.bmp", 0 
EXPLOSION1_PATH 			db 	"explosion1.bmp", 0
EXPLOSION2_PATH 			db 	"explosion2.bmp", 0
MISSILE_PATH 				db 	"missile2.bmp", 0
REDBOMB_PATH 				db 	"redbomb.bmp", 0
REDBOMB2_PATH				db	"redbomb2.bmp", 0
BALLOON_PATH 				db 	"balloon.bmp", 0
BALLOON2_PATH 				db 	"balloon2.bmp", 0
GREENBOMB_PATH 				db 	"greenbomb.bmp", 0
GREENBOMB2_PATH 			db 	"greenbomb2.bmp", 0
TREVASKAS_PATH 				db 	"trevaskas.bmp", 0
PORTAL_PATH 				db 	"portal.bmp", 0
TEZON_PATH				db	"Tezon.bmp", 0
THIRTY_YEARS_PATH			db	"30years.bmp", 0
TREVASKASII_PATH			db	"trevaskasii.bmp", 0
AGAMEBY_PATH				db	"agameby.bmp", 0

; Sound files
SHOTWAVFILE 				db 	"shot.wav", 0
EXPLOSIONWAVFILE 			db 	"explosion1.wav", 0
OPENPORTALWAVFILE			db	"openportal.wav", 0
BACKGROUNDWAVFILE			db	"background.wav", 0
BLIPWAVFILE				db	"blip.wav", 0
CANNONEXPLOSIONWAVFILE			db	"cannonexplosion.wav", 0

; General constants
WINHANDLE 				dq 	0
DD_POINTFOUR 				dd 	0.4
DD_ONE 					dd 	1.0
DQ_ONEPOINTFIVE 			dq 	1.5
DQ_TWO 					dq 	2.0
DQ_FOUR					dq	4.0
DD_TWO 					dd 	2.0
DD_MINUS_TWO 				dd 	-2.0
DQ_EIGHT 				dq 	8.0
DD_ZERO 				dd 	0.0
DQ_ZERO 				dq 	0.0 
SD_HUNDRED				dq	100.0   
DQ_D_SCREENHEIGHT 			dq 	768.0
DQ_I_SCREENWIDTH			dq 	1366
DQ_I_SCREENHEIGHT 			dq 	768
MAX_SHIP_Y				dq	668
START_CANNON_X 				dd 	600.0
START_CANNON_Y 				dd 	700.0
CANNON_XMAX 				dd 	1302.0
CANNON_XMIN 				dd 	0.0
CANNON_ACCELERATION			dq	0.4
PORTAL_CENTER_X 			dq 	683.0
SHOT_OFFSET 				dd 	52.0
START_SHOT_Y 				dq 	664.0
SHOT_YMIN 				dq 	-32.0
SYS_GETTIMEOFDAY 			dq 	96
FPSTARGET 				dq 	100.0
SHOT_SPEED 				dq 	16.0
SHIP_SPEED 				dq 	5.0
TREVASKAS_SPEED 			dq 	2.0
MISSILE_SPEED 				dq 	4.0
REDBOMB_SPEED 				dq 	1.5
MICROSECONDS_FLASH_REDBOMB		equ	20000
GREENBOMB_SPEED 			dq 	1.5
BALLOON_SPEED 				dq 	1.5
MICROSECONDS_FLASH_BALLOON		equ	20000
MICROSECONDS_FRAME_GREENBOMB		equ	1000000
PORTAL_ROTATION_SPEED 			dd 	10.0
TEZON_SPEED				dq	6.0
MAX_CANNON_SPEED			dq	10.0
POINTONE 				dq 	0.1
SHIPSTARTX 				dq 	1000.0 ;start at same value as ship_x  
SHIPSTARTY 				dq 	300.0 ;start at same value as ship_y
SHIPDISTANCE 				dq 	64
ABS_MASK_QWORD 				dq 	0b0111111111111111111111111111111111111111111111111111111111111111
SHIPMIDY 				dq 	10.0
ENEMIES_COUNT 				dq 	20
SHIPS_COUNT 				dq 	8
YOUTOFSCREEN 				dq 	-128.0
SS_X_OUT_OF_SCREEN			dd	1500.0
GREENBOMB_WALK_LENGTH 			dq 	150.0
TEXT_SCALE 				dd 	0.001
DD_XGAMEOVER 				dd 	-0.4
DD_YGAMEOVER 				dd 	0.0
TREVASKAS_X_BOMB 			dd 	50.0
TREVASKAS_Y_BOMB 			dd 	92.0
TREVASKAS_LIFE 				dq 	20
TREVASKAS_MAX_X				dw	1162
TREVASKAS_MAX_Y				dw	512
TREVASKAS_START_X			dd	632.0
TREVASKAS_START_Y			dd	300.0
START_GOALX 				dq 	100.0
START_GOALY 				dq 	300.0
FRAMES_PER_SHOT 			dq 	40    
HEADER_X				dq	20
MICROSECONDS_BETWEEN_SHOTS		dq	400000
DQ_MILLION				dq	1000000
MICROSECONDS_BETWEEN_FLASHES		dq	100000
HALF_MICROSECONDS_BETWEEN_FLASHES	dq	50000
SORTITION_BOMBS_START			dq	800
SORTITION_BOMBS_DECREASE		dq	100
SORTITION_BOMBS_TREVASKAS		equ	40
SYS_write				equ	1
SYS_open 				equ 	2
SYS_close 				equ 	3
SYS_read 				equ 	0
SYS_SCHED_YIELD				equ	24
SYS_EXIT_GROUP				equ	231
O_RDONLY 				equ 	000000q
O_NONBLOCK				equ	0x800
JOYSTICK_PATH				db	"/dev/input/js0", 0
JOYSTICK_RIGHT				dw	16384
JOYSTICK_LEFT				dw	-16384	
TEZON_X					dd	283.0
TEZON_GOAL_Y				dd	100.0
THIRTY_YEARS_X				dd	570.0
THIRTY_YEARS_Y_SHIFT			dd	400.0
TREVASKASII_X				dd	410.0
TREVASKASII_Y				dd	50.0
AGAMEBY_X				dd	481.0
AGAMEBY_Y				dd	310.0
PRESS_ANY_KEY_X				equ	605
PRESS_ANY_KEY_Y				equ	700
SCREEN_OPENING1				equ	1
SCREEN_OPENING2				equ	2
SCREEN_OPENING3				equ	3
SCREEN_GAME				equ	4
MICROSECONDS_PER_FRAME			equ	10000

; gl constants
GL_RGB 					dq 	0x1907
GL_SMOOTH 				dq 	0x1D01               
GL_DEPTH_TEST 				dq 	0x0B71
GL_EQUAL 				dq 	0x0202
GL_PERSPECTIVE_CORRECTION_HINT 		dq 	0x0C50
GL_NICEST 				dq 	0x1102
GL_SRC_ALPHA 				dq 	0x0302
GL_ONE_MINUS_SRC_ALPHA 			dq 	0x0303
GL_BLEND 				dq 	0x0BE2
GL_TEXTURE_ENV 				dq 	0x2300
GL_TEXTURE_ENV_MODE 			dq 	0x2200
GL_DECAL 				dq 	0x2101
GL_LIGHTING 				dq 	0x0B50
GL_TEXTURE_2D 				dq 	3553

; glut constants
GLUT_RGBA 				dq 	0
GLUT_KEY_LEFT 				dq 	100
GLUT_KEY_RIGHT 				dq 	102  
GLUT_KEY_F12 				dq 	12
GLUT_KEY_SPACE 				dq 	32
GLUT_KEY_ESC				dq	27
GLUT_ACTION_ON_WINDOW_CLOSE 		dq 	0x01F9
GLUT_ACTION_GLUTMAINLOOP_RETURNS	dq 	1
GLUT_CURSOR_NONE			equ	101
GLUT_IGNORE_KEY_REPEAT			equ	1

; al constants
AL_BUFFER 				dq 	0x1009
AL_LOOPING				dq	0x1007
    
; General initialized variables
cannon_x 				dd 	600.0
cannon_y 				dd 	700.0
cannon_speed				dq	0.0
trevaskas_goalx 			dd 	500.0
trevaskas_goaly 			dd 	100.0
portal_x 				dd 	621.00
portal_y 				dd 	236.00
portal_rotation 			dd 	0.0
shot_x 					dd 	614.0
shot_y 					dq 	-32.0
ship_x 					dq 	1000.0
ship_y 					dq 	300.0
framecount 				dq 	0
LEFTARROWKEY 				dq 	0
RIGHTARROWKEY 				dq 	0
SPACE_KEY 				dq 	0
background_texturenum 			dq 	0             
delta 					dq 	0.0
playexplode 				dq 	0
playbackground				dq	0
playblip				dq	0
cannon_destroyed 			dq 	0    
score 					dq 	0
stage 					dq 	1
high_score				dq	0
allshipsdestroyed 			dq 	0
trevaskas_hits 				dq 	0
frame_last_shot 			dq 	0
fps_enabled				dq	0
fps_delimiter				dq	0
screen					dq	1
tezon_y					dd	800.0
frame_count_last_second			dq	0
fullscreen				dq	1
first_second				dq	1
cursor_position				dq	0
score_with_label_str			db	"SCORE: ", 0, 0, 0, 0, 0, 0
high_score_with_label_str		db	"HIGH SCORE: ", 0, 0, 0, 0, 0, 0
score_str				db	0, 0, 0, 0, 0, 0
score_strzero_str			db	0, 0, 0, 0, 0, 0
stage_with_label_str			db	"STAGE: ", 0, 0, 0, 0
stage_str				db	0, 0, 0, 0
stage_strzero_str			db	0 ,0, 0, 0
;----------------------------------------------------
section .bss

; Uninitialized variables
texture 				resb 	12288
fpsstr 					resb 	10
scorestr 				resb 	10
stagestr 				resb 	10
high_score_str				resb	10
high_score_strzero_str			resb	10
tempstr					resb	10
tv 					resq 	2
tz 					resq 	2
tv_last_shot				resq	2
tz_last_shot				resq	2
first_tv				resq	2
tv_now					resq	2
tz_now					resq	2
begintime 				resq 	1
currenttime 				resq 	1
shotwavsource 				resq 	1
shotwavbuffer 				resq 	1
explosionsource 			resq 	1
explosionbuffer 			resq 	1
openportalwavsource			resq	1
openportalwavbuffer			resq	1
backgroundwavsource			resq	1
backgroundwavbuffer			resq	1
blipwavsource				resq	1
blipwavbuffer				resq	1
cannonexplosionwavsource		resq	1
cannonexplosionwavbuffer		resq	1
background_width 			resq 	1    
background_height 			resq 	1    
cannon_texnum 				resq 	1
cannon_width 				resq 	1
cannon_height 				resq 	1
shot_texnum 				resq 	1
shot_width 				resq 	1
shot_height 				resq 	1
ships 					resb 	ship_size * 8
enemies 				resb 	enemy_size * ENEMIES_COUNT
ship_texnum 				resq 	1
ship_width 				resq 	1
ship_height 				resq 	1
explosion1_texnum 			resq 	1
explosion1_width 			resq 	1
explosion1_height 			resq 	1
explosion2_texnum 			resq 	1
explosion2_width 			resq 	1
explosion2_height 			resq 	1
missile_texnum 				resq 	1
missile_width 				resq 	1
missile_height 				resq 	1
redbomb_texnum 				resq 	1
redbomb_width 				resq 	1
redbomb_height 				resq 	1
redbomb2_texnum				resq 	1
redbomb2_width 				resq 	1
redbomb2_height				resq 	1
balloon_texnum 				resq 	1
balloon_width 				resq 	1
balloon_height 				resq 	1
balloon2_texnum				resq 	1
balloon2_width 				resq 	1
balloon2_height				resq 	1
greenbomb_texnum 			resq 	1
greenbomb_width 			resq 	1
greenbomb_height 			resq 	1
greenbomb2_texnum 			resq 	1
greenbomb2_width 			resq 	1
greenbomb2_height 			resq 	1
trevaskas_texnum 			resq 	1
trevaskas_width 			resq 	1
trevaskas_height 			resq 	1
portal_texnum 				resq 	1
portal_width 				resq 	1
portal_height 				resq 	1
tezon_texnum				resq	1
tezon_width				resq	1
tezon_height				resq	1
thirty_years_texnum			resq	1
thirty_years_width			resq	1
thirty_years_height			resq	1
trevaskasii_texnum			resq	1
trevaskasii_width			resq	1
trevaskasii_height			resq	1
trevaskas_x 				resd 	1
trevaskas_y 				resd 	1
agameby_texnum				resq	1
agameby_width				resq	1
agameby_height				resq	1
ship_goalx 				resq 	1
ship_goaly 				resq 	1
timecannondestroyed 			resq 	1
first_move 				resq 	1
sortition_bombs				resq	1
joystickdescriptor			resq 	1
joystick				resb	jsevent_size
last_second				resq	1
fps 					resq 	1
align 16, resb 1 
ABS_MASK_OWORD				reso    1
;----------------------------------------------------
section .text

;----------------------------------------------------
;Subroutine from mv_leader_ship
shipnewgoal:	sub rsp, 8
		xor rax, rax
		rdrand ax
		xor rdx, rdx
		mov rcx, qword [DQ_I_SCREENWIDTH]
		sub ecx, dword [ship_width]
		div cx
		cvtsi2sd xmm0, edx
		movq qword [ship_goalx], xmm0
	
		xor rax, rax
		rdrand ax
		xor rdx, rdx
		mov rcx, qword [MAX_SHIP_Y]
		div cx
		cvtsi2sd xmm0, edx
		movq qword [ship_goaly], xmm0
		add rsp, 8
		ret
;----------------------------------------------------
;procedure
mv_leader_ship:	sub rsp, 8
		movq xmm0, qword [ship_y]
		subsd xmm0, [ship_goaly]
		pand xmm0, [ABS_MASK_OWORD]		
		ucomisd xmm0, [DQ_FOUR]
		ja checkgoalxandy
		
		movq xmm0, qword [ship_goaly]
		movq qword [ship_y], xmm0

checkgoalxandy:	movq xmm0, qword [ship_x]
		subsd xmm0, [ship_goalx]
		pand xmm0, [ABS_MASK_OWORD]		
		ucomisd xmm0, [DQ_FOUR]
		ja cont1
				
		movq xmm0, qword [ship_goalx]
		movq qword [ship_x], xmm0
		
		movq xmm0, qword [ship_y]
		subsd xmm0, [ship_goaly]
		pand xmm0, [ABS_MASK_OWORD]		
		ucomisd xmm0, [DQ_FOUR]
		ja cont1		
		

		call shipnewgoal		
						
cont1:		movq xmm2, [delta]
		mulsd xmm2, [SHIP_SPEED]
		movq xmm0, qword [ship_x]
		ucomisd xmm0, [ship_goalx]
		ja goalxabo
		jb goalxbel
		jmp cont2
		
goalxabo:	subsd xmm0, xmm2
		movq qword [ship_x], xmm0
		jmp cont2
		
goalxbel:	addsd xmm0, xmm2
		movq qword [ship_x], xmm0
		
cont2:		movq xmm0, qword [ship_y]
		ucomisd xmm0, [ship_goaly]
		ja goalyabo
		jb goalybel
		jmp mvleaderend
		
goalyabo:	subsd xmm0, xmm2
		movq qword [ship_y], xmm0
		jmp mvleaderend
		
goalybel:	addsd xmm0, xmm2
		movq qword [ship_y], xmm0

mvleaderend:	add rsp, 8
		ret
;----------------------------------------------------		
;procedure
manageshot:	sub rsp, 8
		movq xmm0, [shot_y]
		movq xmm1, [delta]
		mulsd xmm1, [SHOT_SPEED]
		subsd xmm0, xmm1
		movq [shot_y], xmm0
		add rsp, 8
		ret
;----------------------------------------------------		
;procedure
;checks destruction of the cannon
chkcannon:	sub rsp, 8
		mov rax, [cannon_destroyed]
		cmp rax, 1
		jne cont9
		jmp endchkcannon
cont9:		xor rcx, rcx
loop5:		mov rax, rcx
		mov rdx, enemy_size
		mul rdx
		add rax, enemies
		mov rsi, rax
		mov rax, [rsi + enemy.destroyed]
		cmp rax, 1
		je next5
		cvtsi2sd xmm0, qword [redbomb_width]
		divsd xmm0, [DQ_TWO]
		addsd xmm0, [rsi + enemy.x]
		cvtss2sd xmm1, [cannon_x]
		ucomisd xmm0, xmm1
		jb next5
		cvtsi2sd xmm2, qword [cannon_width]
		addsd xmm1, xmm2
		ucomisd xmm0, xmm1
		ja next5
		cvtsi2sd xmm0, qword [redbomb_height]
		divsd xmm0, [DQ_TWO]
		addsd xmm0, [rsi + enemy.y]
		
		cvtss2sd xmm1, [cannon_y]
		cvtsi2sd xmm2, qword [cannon_height]
		divsd xmm2, [DQ_TWO]
		addsd xmm1, xmm2 		
		ucomisd xmm0, xmm1
		jb next5
		
		cvtsi2sd xmm2, qword [cannon_height]
		addsd xmm1, xmm2
		ucomisd xmm0, xmm1
		ja next5
		
		mov qword [cannon_destroyed], 1
		mov rax, [tv]
		mov qword [timecannondestroyed], rax
		
		mov qword [rsi + enemy.destroyed], 1

		mov rax, [score]		
		cmp rax, [high_score]
		jbe playexplwav
		
		mov [high_score], rax
		mov qword [cursor_position], 0
		mov byte [RECORDIST_STR], 0
		mov rdi, RECORDIST_STR
		mov rsi, QUESTION_MARK_STR
		call strcat		
		
playexplwav:	push rcx
		push rcx
		mov rdi, [cannonexplosionwavsource]
		call alSourcePlay wrt ..plt
		pop rcx
		pop rcx
				
next5:		inc rcx
		cmp rcx, [ENEMIES_COUNT]
		jne loop5
endchkcannon:	add rsp, 8
		ret
;----------------------------------------------------		
;procedure
chkenemyshot:	sub rsp, 8
		xor rcx, rcx
loop4:		mov rax, rcx
		mov rdx, enemy_size
		mul rdx
		add rax, enemies
		mov rsi, rax
		mov rax, [rsi + enemy.destroyed]
		cmp rax, 1
		je next3
		
		cvtss2sd xmm0, dword [shot_x]
		movq xmm1, qword [rsi + enemy.x]
		ucomisd xmm0, xmm1
		jb next3
		cvtsi2sd xmm2, qword [missile_width]
		addsd xmm1, xmm2
		ucomisd xmm0, xmm1
		ja next3
		
		movq xmm0, qword [shot_y]
		movq xmm1, qword [rsi + enemy.y]
		ucomisd xmm0, xmm1
		jb next3
		cvtsi2sd xmm2, qword [missile_height]
		addsd xmm1, xmm2
		ucomisd xmm0, xmm1
		ja next3

		mov qword [rsi + enemy.destroyed], 1

		push rcx		
		mov rax, 2
		mov rcx, [rsi + enemy.kind]
		shl rax, cl
		add qword [score], rax			
		pop rcx
		
		mov qword [playexplode], 1
		mov rax, [YOUTOFSCREEN]
		mov qword [shot_y], rax				
		mov rax, [tv]
		mov qword [rsi + enemy.timedestroyed], rax
		jmp end4		
next3:		inc rcx
		cmp rcx, [ENEMIES_COUNT]
		jne loop4
end4:		add rsp, 8
		ret		
;----------------------------------------------------		
chkshottrevask:	sub rsp, 8
		movd xmm0, dword [trevaskas_x]
		movd xmm1, dword [shot_x]
		ucomiss xmm1, xmm0
		jb endchkshttre
		cvtsi2ss xmm2, dword [trevaskas_width]
		addss xmm0, xmm2
		ucomiss xmm1, xmm0
		ja endchkshttre
		movd xmm0, dword [trevaskas_y]
		cvtsd2ss xmm1, [shot_y]
		ucomiss xmm1,xmm0
		jb endchkshttre
		cvtsi2ss xmm2, dword [trevaskas_height]
		addss xmm0, xmm2
		ucomiss xmm1, xmm0
		ja endchkshttre
		mov rsi, enemies
		mov qword [rsi + enemy.destroyed], 1
		add qword [score], 5
		mov qword [playexplode], 1
		mov rax, [tv]
		mov qword [rsi + enemy.timedestroyed], rax
		movd xmm0, dword [shot_x]
		cvtss2sd xmm1, xmm0
		movq qword [rsi + enemy.x], xmm1
		movq xmm0, [shot_y]
		movq [rsi + enemy.y], xmm0
		mov rax, [YOUTOFSCREEN]		
		mov qword [shot_y], rax
		inc qword [trevaskas_hits]		
endchkshttre:	add rsp, 8
		ret
;----------------------------------------------------		
;Load time to global named "tv"
loadtime:	sub rsp, 8
		xor rax, rax
		mov [tv], rax
		mov [tv + 8], rax
		mov [tz], rax
		mov [tz + 8], rax
		mov rax, [SYS_GETTIMEOFDAY]
		mov rdi, tv
		mov rsi, tz
		syscall
		add rsp, 8
		ret		
;----------------------------------------------------		
;Calculate globals delta and fps
calcimfpsdelta:	sub rsp, 8
		
		mov rax, [tv]
		cmp rax, [last_second]
		je endcalcimfps
		mov [last_second], rax
		mov rax, [frame_count_last_second]
		
		; FPS must be greater than zero to avoid division by zero
		cmp rax, 0
		jne setfps
		mov rax, qword 1
		
setfps:		mov [fps], rax
		mov qword [frame_count_last_second], 0
		
		movq xmm0, [FPSTARGET]
		cvtsi2sd xmm1, eax
		divsd xmm0, xmm1
		movq [delta], xmm0		
		
endcalcimfps:	inc qword [frame_count_last_second]
		add rsp, 8
		ret
;----------------------------------------------------				
handlearrows:	sub rsp, 8

		movq xmm0, [cannon_speed]
		movq xmm1, [CANNON_ACCELERATION]
		mulsd xmm1, [delta]
		addsd xmm0, xmm1
		ucomisd xmm0, [MAX_CANNON_SPEED]
		jbe calcspeed
		movq xmm0, [MAX_CANNON_SPEED]
		
calcspeed:	movq [cannon_speed], xmm0
		movq xmm1, [delta]
		mulsd xmm1, [cannon_speed]
		cvtsd2ss xmm1, xmm1

		cmp qword [RIGHTARROWKEY], 1
		jne hasfps2
		movd xmm0, dword [cannon_x]
		addss xmm0, xmm1
		ucomiss xmm0, dword [CANNON_XMAX]
		ja endhndlarrws
		movd dword [cannon_x], xmm0	
	
hasfps2:	cmp qword [LEFTARROWKEY], 1
		jne endhndlarrws
		movd xmm0, dword [cannon_x]
		subss xmm0, xmm1
		ucomiss xmm0, dword [CANNON_XMIN]
		jb endhndlarrws
		movd dword [cannon_x], xmm0	
endhndlarrws:	add rsp, 8
		ret
;----------------------------------------------------		
handlespace:	sub rsp, 8
		cmp qword [SPACE_KEY], 1
		jne endhlndspc
		movq xmm0, qword [shot_y]
		ucomisd xmm0, [SHOT_YMIN]
		ja endhlndspc
		mov rax, [first_move]
		cmp rax, 1
		je endhlndspc
				
		mov rax, [tv]
		mov rcx, [tv_last_shot]
		sub rax, rcx
		cmp rax, 2
		jge donewshot
		
		mov rax, [tv + 8]
		mov rcx, [tv_last_shot + 8]
		cmp rax, rcx
		jg calcdif
		add rax, [DQ_MILLION]
calcdif:	sub rax, rcx
		cmp rax, [MICROSECONDS_BETWEEN_SHOTS]	
		jl endhlndspc	
		
donewshot:	movq xmm0, [START_SHOT_Y]
		movq [shot_y], xmm0
		movd xmm0, dword [cannon_x]
		addss xmm0, dword [SHOT_OFFSET]
		movd dword [shot_x], xmm0
		mov rax, [framecount]
		mov [frame_last_shot], rax
		
		xor rax, rax
		mov [tv_last_shot], rax
		mov [tv_last_shot + 8], rax
		mov [tz_last_shot], rax
		mov [tz_last_shot + 8], rax
		mov rax, [SYS_GETTIMEOFDAY]
		mov rdi, tv_last_shot
		mov rsi, tz_last_shot
		syscall
				
		mov rdi, [shotwavsource]
		call alSourcePlay wrt ..plt
endhlndspc:	add rsp, 8
		ret
;----------------------------------------------------		
checkplayexpl:	sub rsp, 8
		cmp qword [playexplode], 1
		jne endchkplexpl
		mov rdi, qword [explosionsource]
		call alSourcePlay wrt ..plt		
		mov qword [playexplode], 0
endchkplexpl:	add rsp, 8
		ret
;----------------------------------------------------		
checkplaybkg:	sub rsp, 8
		cmp qword [playbackground], 1
		jne endchkplbkg
		mov rdi, qword [backgroundwavsource]
		call alSourcePlay wrt ..plt		
		mov qword [playbackground], 0
endchkplbkg:	add rsp, 8
		ret
;----------------------------------------------------		
checkplayblip:	sub rsp, 8
		cmp qword [playblip], 1
		jne endchkplblp
		mov rdi, qword [blipwavsource]
		call alSourcePlay wrt ..plt		
		mov qword [playblip], 0
endchkplblp:	add rsp, 8
		ret
;----------------------------------------------------		
;subroutine from onidle
readjoystick:	sub rsp,8
looprdjystck:	mov rax, SYS_read
		mov rdi, qword [joystickdescriptor]
		mov rsi, joystick
		mov rdx, jsevent_size
		syscall    
		
		cmp rax, 0
		jl endrdjs
				
		xor rcx, rcx
		mov cl, byte [rsi + jsevent.etype]				
		cmp cl, 2
		jne chkjsbuttons
		
		mov rdx, [cannon_destroyed]
		cmp rdx, 1
		je chkjsbuttons
				
		mov dx, word [rsi + jsevent.value]		
		cmp dx, [JOYSTICK_RIGHT]
		jle notjsright
		
		mov qword [RIGHTARROWKEY], 1
		pxor xmm0, xmm0
		movq [cannon_speed], xmm0
		jmp nextjsevent
		
notjsright:	cmp dx, [JOYSTICK_LEFT]
		jle moveleft		
		mov qword [RIGHTARROWKEY], 0
		mov qword [LEFTARROWKEY], 0
		jmp nextjsevent
		
moveleft:	mov qword [LEFTARROWKEY], 1
		pxor xmm0, xmm0
		movq [cannon_speed], xmm0
		jmp nextjsevent
		
chkjsbuttons:	cmp cl, 1
		jne nextjsevent
				
chkbuttons:	xor rdx, rdx
		mov dl, [rsi + jsevent.number]
		cmp dl, 1
		jne nextjsevent
		mov dx, word [rsi + jsevent.value]		
		cmp dx, 1
		jne breleased
		
		cmp qword [cannon_destroyed], 1
		jne bpressed
		call setnewgame
		jmp endrdjs				
		
bpressed:	mov rax, [screen]
		cmp rax, SCREEN_GAME
		je setspace
		call manscrchng
setspace:	mov qword [SPACE_KEY], 1
		jmp nextjsevent		
breleased:	mov qword [SPACE_KEY], 0		
						
nextjsevent:	jmp looprdjystck
		
endrdjs:	add rsp, 8
		ret
;----------------------------------------------------		
;Subroutine from onidle
;Manage opening 1
man_opening1:	sub rsp, 8

		;Do not move during the first two seconds - waiting to calculate the first FPS
		cmp qword [first_second], 1
		jne movetezon
		mov rax, [first_tv]
		add rax, 2
		cmp rax, [last_second]
		jg endmnopng1
		mov qword [first_second], 0
		
movetezon:	movd xmm0, dword [tezon_y]
		ucomiss xmm0, dword [TEZON_GOAL_Y]
		jb endmnopng1
		movq xmm1, [delta]
		mulsd xmm1, [TEZON_SPEED]
		cvtsd2ss xmm2, xmm1		
		subss xmm0, xmm2
		
		ucomiss xmm0, dword [TEZON_GOAL_Y]
		jae settezony
		movd xmm0, dword [TEZON_GOAL_Y]
		
settezony:	movd dword [tezon_y], xmm0
endmnopng1:	add rsp, 8
		ret		
;----------------------------------------------------		
onidle:		push rbp
		push rbx
		sub rsp, 8

		call loadtime            
		call calcimfpsdelta
		call readjoystick
		inc qword [framecount]

		mov rax, [screen]
		cmp rax, SCREEN_OPENING1
		jne chkscopen2
		call man_opening1
		jmp cont5

chkscopen2:	cmp rax, SCREEN_OPENING2
		jne chkscopen3
		jmp cont5

chkscopen3:	cmp rax, SCREEN_OPENING3
		jne chkcndstrd
		jmp cont5

chkcndstrd:	mov rax, [cannon_destroyed]
		cmp rax, 1
		je continue1 
		call handlearrows
		call handlespace		
continue1:	call manageshot
		call mv_leader_ship		
		mov rax, [stage]
		mov bl, 20
		div bl
		cmp ah, 0
		je notmoveships
		call move_ships
notmoveships:	call move_enemies
		call chkenemyshot
		call chkcannon
		
		mov rax, [stage]
		mov bl, 20
		div bl
		cmp ah, 0
		jne notrevaskas
		mov rax, [trevaskas_hits]
		cmp rax, [TREVASKAS_LIFE]
		jge waitstgaftertre
		call man_trevaskas
		call chkshottrevask

		mov rax, [trevaskas_hits]
		cmp rax, [TREVASKAS_LIFE]
		jl cont12
		
waitstgaftertre:	call chknewstage
		cmp rax, 1
		jne cont12
		mov qword [allshipsdestroyed], 0
		inc qword [stage]		
		call create_ships		

notrevaskas:	call chknewstage
		cmp rax, 1
		jne cont12
		mov rax, [allshipsdestroyed]
		cmp rax, 1
		jne cont12
		mov qword [allshipsdestroyed], 0
		inc qword [stage]
		mov rax, [stage]
		mov bl, 20
		div bl
		cmp ah, 0
		je cont12
		
		call create_ships		

cont12:		call checkplayexpl
		call checkplaybkg
		call checkplayblip
		
cont5:		call glutPostRedisplay wrt ..plt
		add rsp, 8
		pop rbx
		pop rbp
		ret
;----------------------------------------------------
;Subroutine from draw
;draw cannon exploding		
drcannonexpl:	sub rsp, 8
		mov rdi, [tv]
		sub rdi, [timecannondestroyed]
		cmp rdi, 2
		jb drawcannon
		movd xmm0, [SS_X_OUT_OF_SCREEN]
		movd dword [cannon_x], xmm0		
		jmp endcannonexpl
drawcannon:	movd xmm0, dword [cannon_x]
		movd xmm1, dword [cannon_y]
		cvtsi2ss xmm2, dword [cannon_width]
		cvtsi2ss xmm3, dword [cannon_height]
		xor rdx, rdx
		mov rax, [tv + 8]
		mov rdi, [MICROSECONDS_BETWEEN_FLASHES]
		div rdi
		cmp rdx, [HALF_MICROSECONDS_BETWEEN_FLASHES]
		ja explosion4
		mov rdi, [explosion1_texnum]
		jmp drawexpl3
explosion4:	mov rdi, [explosion2_texnum]
drawexpl3:	call drawspr
endcannonexpl:	add rsp, 8
		ret		
;----------------------------------------------------		
drawbackground:	sub rsp, 8
		movd xmm0, dword [DD_ZERO]
		movd xmm1, dword [DD_ZERO]
		cvtsi2ss xmm2, dword [background_width]
		cvtsi2ss xmm3, dword [background_height]
		mov rdi, [background_texturenum]
		call drawspr		
		add rsp, 8
		ret		
;----------------------------------------------------
yield:		sub rsp, 8

checkyield:	mov rax, [SYS_GETTIMEOFDAY]
		mov rdi, tv_now
		mov rsi, tz_now
		syscall
		
		mov rax, [tv_now]
		cmp rax, [tv]
		jne endyield
		
		mov rax, [tv_now + 8]
		sub rax, [tv + 8]
		cmp rax, MICROSECONDS_PER_FRAME
		jge endyield

		mov rax, SYS_SCHED_YIELD
		syscall
		
		jmp checkyield

endyield:	add rsp, 8
		ret
;----------------------------------------------------
draw:		push rbp
		push rbx
		sub rsp, 8
		
		mov rax, [screen]
		cmp rax, SCREEN_OPENING1
		jne checkopening2
		call draw_opening1
		jmp enddraw
		
checkopening2:	cmp rax, SCREEN_OPENING2
		jne checkopening3
		call draw_opening2
		jmp enddraw
		
checkopening3:	cmp rax, SCREEN_OPENING3
		jne drawbckgrnd
		call draw_opening3
		jmp enddraw		
		
drawbckgrnd:	call drawbackground

		mov rax, [stage]
		mov bl, 20
		div bl
		cmp ah, 0
		jne nodrawtrevaskas
		mov rax, [trevaskas_hits]
		cmp rax, [TREVASKAS_LIFE]
		je nodrawtrevaskas
	
		movd xmm0, dword [trevaskas_x]
		movd xmm1, dword [trevaskas_y]	
		cvtsi2ss xmm2, dword [trevaskas_width]
		cvtsi2ss xmm3, dword [trevaskas_height]
		mov rdi, [trevaskas_texnum]
		call drawspr	
	
nodrawtrevaskas:call draw_hud	
	
		mov rax, [cannon_destroyed]
		cmp rax, 1
		je cont7
		
		movd xmm0, dword [cannon_x]
		movd xmm1, dword [cannon_y]
		cvtsi2ss xmm2, dword [cannon_width]
		cvtsi2ss xmm3, dword [cannon_height]
		mov rdi, [cannon_texnum]
		call drawspr
		jmp cont8	

cont7:		call drcannonexpl

cont8:		movd xmm0, dword [shot_x]
		cvtsd2ss xmm1, [shot_y]
		cvtsi2ss xmm2, dword [shot_width]
		cvtsi2ss xmm3, dword [shot_height]
		mov rdi, [shot_texnum]
		call drawspr
	
		call draw_portal					
		call draw_ships
		call draw_enemies
	
enddraw:	call glFlush wrt ..plt

		cmp qword [fps_delimiter], 0
		je end2draw
		call yield

end2draw	add rsp, 8
		pop rbx
		pop rbp
		ret
;----------------------------------------------------
dospeckey:	push rbp
		push rbx
		sub rsp, 8

		cmp rdi, [GLUT_KEY_RIGHT]
		je addx
		cmp rdi, [GLUT_KEY_LEFT]
		je subx
		jmp end_dospeckey
addx:		mov qword [RIGHTARROWKEY], 1
		pxor xmm0, xmm0
		movq [cannon_speed], xmm0
		jmp end_dospeckey
subx:		mov qword [LEFTARROWKEY], 1
		pxor xmm0, xmm0
		movq [cannon_speed], xmm0

end_dospeckey:	add rsp, 8
		pop rbx
		pop rbp
		ret

;----------------------------------------------------
doupspeckey:	sub rsp, 8
		cmp rdi, [GLUT_KEY_RIGHT]
		je upright
		cmp rdi, [GLUT_KEY_LEFT]
		je upleft
		cmp rdi, [GLUT_KEY_F12]
		je fpsswitch
		jmp end_upspeckey
upright:	mov qword [RIGHTARROWKEY], 0
		jmp end_upspeckey
fpsswitch:	not qword [fps_enabled]
                jmp end_upspeckey
upleft:		mov qword [LEFTARROWKEY], 0

end_upspeckey:	add rsp, 8
		ret
;----------------------------------------------------    
;check if the stage can be reset. Returns 1 if there is no enemy alive or exploding
chknewstage:	sub rsp, 8
		xor rcx, rcx
loop6:		mov rax, rcx
		mov rdx, enemy_size
		mul rdx
		add rax, enemies
		mov rsi, rax
		mov rax, [rsi + enemy.destroyed]
		cmp rax, 0
		je retfalse
		mov rax, [tv]
		sub rax, [rsi + enemy.timedestroyed]
		cmp rax, 2
		jb retfalse								
		inc rcx
		cmp rcx, [ENEMIES_COUNT]
		jne loop6
		xor rcx, rcx
loop7:		mov rax, rcx
		mov rdx, ship_size
		mul rdx
		add rax, ships	
		mov rsi, rax
		mov rax, [tv]
		sub rax, [rsi + ship.timedestroyed]
		cmp rax, 2
		jb retfalse
		inc rcx
		cmp rcx, [SHIPS_COUNT]
		jne loop7		
		mov rax, 1
		jmp endchkcanrst		
retfalse:	mov rax, 0
endchkcanrst:	add rsp, 8
		ret
;----------------------------------------------------    
setnewgame:	sub rsp, 8
		mov eax, [START_CANNON_X]
		mov dword [cannon_x], eax
		mov eax, [START_CANNON_Y]
		mov dword [cannon_y], eax
		mov eax, [TREVASKAS_START_X]
		mov dword [trevaskas_x], eax
		mov eax, [TREVASKAS_START_Y]
		mov dword [trevaskas_y], eax
		mov qword [stage], 1
		mov qword [score], 0
		mov qword [cannon_destroyed], 0
		mov qword [RIGHTARROWKEY], 0
		mov qword [LEFTARROWKEY], 0
		mov qword [SPACE_KEY], 0		
		mov qword [trevaskas_hits], 0
		call create_ships
		call create_enemies
		add rsp, 8
		ret
;----------------------------------------------------    
; Routine that manage screen change in response of space or joystick button pressed
manscrchng:	sub rsp,8
		mov rax, [screen]
		cmp rax, SCREEN_OPENING3
		je setscreengame
		
		cmp rax, SCREEN_OPENING1
		jne chkpng2
		
		mov qword [screen], SCREEN_OPENING2
		jmp endmanscrchng

chkpng2:	cmp rax, SCREEN_OPENING2
		jne endmanscrchng

		mov qword [screen], SCREEN_OPENING3
		jmp endmanscrchng

setscreengame:	mov qword [screen], SCREEN_GAME		
		call inittime
		mov qword [framecount], 0
endmanscrchng:	add rsp, 8
		ret
;----------------------------------------------------    
dokey:		sub rsp, 8

chkesc:		cmp rdi, [GLUT_KEY_ESC]
		jne chkcnnndstrd
		call glutLeaveMainLoop wrt ..plt
		jmp enddokey

chkcnnndstrd:	cmp qword [cannon_destroyed], 1
		jne chkspace
		cmp rdi, [GLUT_KEY_SPACE]		
		jne chkspace
		call setnewgame
		jmp enddokey
				
chkspace:	cmp rdi, [GLUT_KEY_SPACE]
		jne checkotherkeys

		mov rax, [screen]
		cmp rax, SCREEN_GAME
		je spacepressed
		
		call manscrchng
				
spacepressed:	mov qword [SPACE_KEY], 1
		cmp qword [cursor_position], 0
		je enddokey
		jmp updtrecord

checkotherkeys:	cmp rdi, 65 ;'a'
		jb enddokey
		cmp rdi, 122 ;'Z'
		ja enddokey
updtrecord:	mov rax, [cursor_position]
		mov [RECORDIST_STR + rax], dil
		cmp qword [cursor_position], 7
		jae enddokey
		inc qword [cursor_position] 
				
enddokey:	add rsp, 8
		ret
;----------------------------------------------------
doupkey:	sub rsp, 8
		cmp rdi, [GLUT_KEY_SPACE]
		jne enddoupkey
		mov qword [SPACE_KEY], 0
enddoupkey:	add rsp, 8
		ret		
;----------------------------------------------------
;Subroutine from move_enemies
move_balloon:	sub rsp, 8
		addsd xmm0, xmm6
		cvtss2sd xmm8, [cannon_x]
		cvtss2sd xmm9, [SHOT_OFFSET]
		addsd xmm8, xmm9		
		movq xmm4, qword [rsi + enemy.x]
		movq xmm9, qword [redbomb_width]
		divsd xmm9, [DQ_TWO]
		addsd xmm4, xmm9		
		ucomisd xmm4, xmm8
		ja goleftballoon
		addsd xmm4, xmm6
		jmp putswing 
goleftballoon:	subsd xmm4, xmm6
putswing:	movq xmm7, xmm6
		mulsd xmm7, [DQ_ONEPOINTFIVE]
		mov rax, [tv + 8] ;get microseconds
		xor rdx, rdx
		mov r8, 250000
		div r8
		test al, 1
		jz even
		addsd xmm4, xmm7
		jmp exitmvbln
even:		subsd xmm4, xmm7
exitmvbln:	movq qword [rsi + enemy.x], xmm4
		add rsp, 8
		ret
;----------------------------------------------------
;Subroutine from move_enemies
move_greenbomb:	sub rsp, 8
		movq xmm4, qword [DQ_D_SCREENHEIGHT]
		cvtsi2sd xmm5, qword [greenbomb_height]
		subsd xmm4, xmm5
		ucomisd xmm0, xmm4
		jb move_down

		movq xmm8, qword [rsi + enemy.xorigin]
		subsd xmm8, [rsi + enemy.x]
		pand xmm8, [ABS_MASK_OWORD]
		ucomisd xmm8, [GREENBOMB_WALK_LENGTH]
		ja destroy_grnbmb		

		cvtss2sd xmm8, [cannon_x]
		movq xmm4, qword [rsi + enemy.x]
		ucomisd xmm4, xmm8
		ja goleft2
		addsd xmm4, xmm2
		movq qword [rsi + enemy.x], xmm4
		jmp end_grnbmb 
goleft2:	subsd xmm4, xmm2
		movq qword [rsi + enemy.x], xmm4
		jmp end_grnbmb		
destroy_grnbmb:	mov qword [rsi + enemy.destroyed], 1		
move_down:	addsd xmm0, xmm3
end_grnbmb:	add rsp, 8
		ret
;----------------------------------------------------
move_enemies:	sub rsp, 8
		xor rcx, rcx
		movq xmm1, [delta]
		mulsd xmm1, [MISSILE_SPEED]
		movq xmm2, [delta]
		mulsd xmm2, [REDBOMB_SPEED]
		movq xmm3, [delta]
		mulsd xmm3, [GREENBOMB_SPEED]
		movq xmm6, [delta]
		mulsd xmm6, [BALLOON_SPEED]
loop_enemies:	mov rax, rcx
		mov rdx, enemy_size
		mul rdx
		add rax, enemies
		mov rsi, rax
		mov rax, [rsi + enemy.destroyed]
		cmp rax, 0
		jne next_enemy
		movq xmm0, qword [rsi + enemy.y]
		mov rax, [rsi + enemy.kind]
		cmp rax, 0
		je move_missile
		cmp rax, 2
		je mv_grnbmb
		cmp rax, 3
		je mv_balloon
		addsd xmm0, xmm2
		cvtss2sd xmm8, [cannon_x]
		cvtss2sd xmm9, [SHOT_OFFSET]
		addsd xmm8, xmm9
		movq xmm4, qword [rsi + enemy.x]
		movq xmm9, [redbomb_width]
		divsd xmm9, [DQ_TWO]
		addsd xmm4, xmm9
		ucomisd xmm4, xmm8
		ja goleft
		addsd xmm4, xmm2
		movq qword [rsi + enemy.x], xmm4
		jmp cont11 
goleft:		subsd xmm4, xmm2
		movq qword [rsi + enemy.x], xmm4
		jmp cont11
move_missile:	addsd xmm0, xmm1
		jmp cont11
		
mv_balloon:	call move_balloon
		jmp cont11		
				
mv_grnbmb:	call move_greenbomb

cont11:		movq qword [rsi + enemy.y], xmm0
		ucomisd xmm0, [DQ_D_SCREENHEIGHT]
		jb next_enemy
destroy_enemy:	mov qword [rsi + enemy.destroyed], 1
		
next_enemy:	inc rcx
		cmp rcx, [ENEMIES_COUNT]
		jne loop_enemies
		add rsp, 8
		ret
;----------------------------------------------------]
;subroutine from checkcolshot		
checkhit:	sub rsp, 8
		cvtsi2sd xmm12, qword [SHIPDISTANCE]
             	divsd xmm12, [DQ_TWO]
             	movq xmm14, xmm0
		addsd xmm14, xmm12
		cvtss2sd xmm13, dword [shot_x]
		subsd xmm14, xmm13
		pand xmm14, [ABS_MASK_OWORD]
		ucomisd xmm14, xmm12
		ja nohit
		movq rax, xmm1
		addsd xmm1, [SHIPMIDY]
		movq xmm13, [shot_y]
		movq xmm14, xmm1
		movq xmm1, rax
		subsd xmm14, xmm13
		pand xmm14, [ABS_MASK_OWORD]
		ucomisd xmm14, [SHIPMIDY]
		jb hit
nohit:		mov rax,0
		jmp endcheckhit
hit:		mov qword [rsi + ship.destroyed], 1
		mov rax, 1
endcheckhit:	add rsp, 8
		ret		
;----------------------------------------------------
;subroutine from onidle
checkcolshot:	sub rsp, 8
		mov rax, qword [rsi + ship.destroyed]
		cmp rax, 1
		je end
		
		call checkhit
		cmp rax,0
		je end
		
		mov qword [playexplode], 1
		add qword [score], 5

		mov rax, [tv]
		mov qword [rsi + ship.timedestroyed], rax
		mov rax, [YOUTOFSCREEN]
		mov qword [shot_y], rax
		
		mov rax, [SORTITION_BOMBS_DECREASE]
		sub [sortition_bombs], rax		
		
		;ship behind the destroyed change its prior to prior of the destroyed ship
		;check if is not the last ship
		mov rax, [rsi + ship.next]
		mov r11, rax
		cmp rax, 8
		je end
		mov r10, [rsi + ship.prior]
		mov rdx, ship_size
		mul rdx
		add rax, ships
		mov [rax + ship.prior], r10
		movd [rax + ship.goalx], xmm2
		movd [rax + ship.goaly], xmm3
		
		;if prior is under zero, set coordinates and skip change prior ship
		cmp r10, 0
		jge cont15
		movq xmm15, [rax + ship.x]
		movq [ship_x], xmm15
		movq xmm15, [rax + ship.y]
		movq [ship_y], xmm15		
		jmp end
		
		;change next into prior ship
cont15:		mov rax, r10
		mov rdx, ship_size
		mul rdx
		add rax, ships
		mov [rax + ship.next], r11 
		
end:		add rsp, 8
		ret
;----------------------------------------------------
;Subroutine from man_trevaskas
setnewcoords:	sub rsp, 8
		xor rax, rax
		rdrand ax
		xor rdx, rdx
		mov cx, [TREVASKAS_MAX_X]
		div cx
		cvtsi2ss xmm0, edx
		movd dword [trevaskas_goalx], xmm0
	
		xor rax, rax
		rdrand ax
		xor rdx, rdx
		mov cx, [TREVASKAS_MAX_Y]
		div cx
		cvtsi2ss xmm0, edx
		movd dword [trevaskas_goaly], xmm0
		add rsp, 8
		ret
;----------------------------------------------------
man_trevaskas:	;Calculate delta distance
		sub rsp, 8
		movq xmm0, qword [delta]
		mulsd xmm0, [TREVASKAS_SPEED]
		addsd xmm0, qword [POINTONE]
		cvtsd2ss xmm0, xmm0
		
		;goalx - x
		movd xmm1, [trevaskas_x]
		movd xmm2, [trevaskas_goalx]
		subss xmm2, xmm1
				
		;if (goalx - x) > 2 then x = x + delta 
		ucomiss xmm2, [DD_TWO]
		jb cont13
		addss xmm1, xmm0
		
		;if (goalx -x) < -2 then x = x - delta
cont13:		ucomiss xmm2, [DD_MINUS_TWO]
		ja checkytre
		subss xmm1, xmm0
		
checkytre:	;goaly - y
		movd xmm3, [trevaskas_y]
		movd xmm4, [trevaskas_goaly]
		subss xmm4, xmm3

		;if (goaly -y) > 2 then y = y + delta
		ucomiss xmm4, [DD_TWO]
		jb cont14
		addss xmm3, xmm0
		
		;if (goaly - y) < -2 then y = y - delta
cont14:		ucomiss xmm4, [DD_MINUS_TWO]
		ja chkneednew
		subss xmm3, xmm0 	

		;if xmm1 <> trevaskas_x 
chkneednew:	ucomiss xmm1, [trevaskas_x]
		jne endmantre	
		ucomiss xmm3, [trevaskas_y]
		jne endmantre

		call setnewcoords		
				
endmantre:	movd [trevaskas_x], xmm1		
		movd [trevaskas_y], xmm3
		
		addss xmm1, [TREVASKAS_X_BOMB]
		cvtss2sd xmm1, xmm1
		movq rdi,xmm1
		addss xmm3, [TREVASKAS_Y_BOMB]
		cvtss2sd xmm3, xmm3
		movq rsi, xmm3
		mov rdx, SORTITION_BOMBS_TREVASKAS
		call checknewenemy		
		
		add rsp, 8		
		ret		
;----------------------------------------------------		
;Update first non-destroyed ship
;Subroutine from move_ships
updtfrstshp:	sub rsp, 8
		xor rcx, rcx
loop2:		mov rax, rcx
		mov rdx, ship_size
		mul rdx
		add rax, ships	
		mov rsi, rax
		
		;if is destroyed, go to next ship
		mov rax, [rsi + ship.destroyed]
		cmp rax, 1
		jne cont4
		inc rcx
		cmp rcx, 8
		je shipsdest
		jmp loop2
		
shipsdest:	mov qword [allshipsdestroyed], 1
		jmp end5
		
cont4:		mov rax, [ship_x]
		mov [rsi + ship.x], rax
		mov rax, [ship_y]
		mov [rsi + ship.y], rax
		mov rax, [ship_goalx]
		mov [rsi + ship.goalx], rax
		mov rax, [ship_goaly]
		mov [rsi + ship.goaly], rax

		mov rax, rcx
		mov rdx, ship_size
		mul rdx
		add rax, ships	
		mov rsi, rax
		movq xmm0, qword [rsi + ship.x]
		movq xmm1, qword [rsi + ship.y]
		movq xmm10, qword [rsi + ship.goalx]
		movq xmm11, qword [rsi + ship.goaly]		
		
		push rsi
		push rcx
		movq rdi, xmm0
		movq rsi, xmm1
		mov rdx, [sortition_bombs]
		call checknewenemy
		pop rcx
		pop rsi		
		
end5:		add rsp, 8
		ret		
;----------------------------------------------------
; Subroutine from move_ships
; Fill registers with ship data
fillregshipdata:sub rsp, 8
		movq xmm0, qword [rsi + ship.x]
		movq xmm1, qword [rsi + ship.y]
		movq xmm10, qword [rsi + ship.goalx]
		movq xmm11, qword [rsi + ship.goaly]		
		
		;Fill registers with prior ship data
		mov rax, [rsi + ship.prior]
		mov rdx, ship_size
		mul rdx
		add rax, ships	
		movq xmm2, qword [rax + ship.x]
		movq xmm3, qword [rax + ship.y]
		movq xmm8, qword [rax + ship.goalx]
		movq xmm9, qword [rax + ship.goaly]
		
		;if goalx and goaly is 0, get from prior
		ucomisd xmm10, [DQ_ZERO]
		jnz end6
		ucomisd xmm11, [DQ_ZERO]
		jnz end6		
		movq xmm10, xmm8
		movq xmm11, xmm9
		movq qword [rsi + ship.goalx], xmm10
		movq qword [rsi + ship.goaly], xmm11
end6:		add rsp, 8
		ret
;----------------------------------------------------
; Subroutine from move_ships
; Add delta to ship coordinates
inccoords:	;if abs(x - xprior) > shipdistance then goto continue	
		sub rsp, 8
		movsd xmm4, xmm0
		subsd xmm4, xmm2
		pand xmm4, [ABS_MASK_OWORD]
		cvtsi2sd xmm5, qword [SHIPDISTANCE]
		ucomisd xmm4, xmm5
		ja continue		

		;if abs(y - yprior) < shipdistance then goto savecoords
		movsd xmm4, xmm1
		subsd xmm4, xmm3
		pand xmm4, [ABS_MASK_OWORD]
		cvtsi2sd xmm5, qword [SHIPDISTANCE]
		ucomisd xmm4, xmm5
		jb end7		
		
		;if x > goalx then goto decxship else goto incxship
continue:	ucomisd xmm0, xmm10
		ja decxship
		jb incxship
		jmp compareys
		
decxship:	;subtract delta from x
		subsd xmm0, xmm7
		jmp compareys
		
incxship:	;add delta to x	
		addsd xmm0, xmm7
				
		;if y > goaly then goto decyship else goto incyship
compareys:	ucomisd xmm1, xmm11
		ja decyship
		jb incyship
		jmp end7

		;subtract delta from y		
decyship:	subsd xmm1, xmm7
		jmp end7
		
incyship:	;add delta to y
		addsd xmm1, xmm7

		;if abs(goaly-y) < 4 then y = goaly
end7:		movsd xmm15, xmm1
		subsd xmm15, xmm11	
		pand xmm15, [ABS_MASK_OWORD]			
		ucomisd xmm15, [DQ_FOUR]
		ja checkgoalx
		movsd xmm1, xmm11

		;if abs(goalx-x) < 4 then x = goalx
checkgoalx:	movsd xmm15, xmm0
		subsd xmm15, xmm10	
		pand xmm15, [ABS_MASK_OWORD]			
		ucomisd xmm15, [DQ_FOUR]
		ja endinccoords
		movsd xmm0, xmm10

endinccoords:	add rsp, 8
		ret
;---------------------------------------------------------------
; Subroutine from move_ships
; If reached the goal, pull the goal from prior		
getnextgoal:	sub rsp,8
		subsd xmm0, xmm10		
		pand xmm0, [ABS_MASK_OWORD]
		ucomisd xmm0, [DQ_EIGHT]
		ja end8
		subsd xmm1, xmm11
		pand xmm1, [ABS_MASK_OWORD]
		ucomisd xmm1, [DQ_EIGHT]
		ja end8
		movq qword [rsi + ship.goalx], xmm2
		movq qword [rsi + ship.goaly], xmm3
		mov rax, [first_move]
		cmp rax, 1
		jne notstartbkgsnd
		mov qword [playbackground], 1
notstartbkgsnd:	mov qword [first_move], 0		
end8:		add rsp, 8
		ret
;----------------------------------------------------
; procedure
move_ships:	;Calculate delta - if delta < 0 then delta := 1
		sub rsp, 8
		movq xmm7, [delta]
		mulsd xmm7, [SHIP_SPEED]
		movq xmm0, xmm7
		divsd xmm0, [SD_HUNDRED]
		addsd xmm7, xmm0

		call updtfrstshp		
		cmp qword [allshipsdestroyed], 1		
		je end3
		
		call checkcolshot		
		
		inc rcx
		cmp rcx, 8
		je end3
		
loopmoveships:
		mov rax, rcx
		mov rdx, ship_size
		mul rdx
		add rax, ships	
		mov rsi, rax
		
		mov rax, [rsi + ship.destroyed]
		cmp rax, 1
		je nextship		

		call fillregshipdata		
				
cont:		call inccoords
		
savecoords:
		movq qword [rsi + ship.x], xmm0
		movq qword [rsi + ship.y], xmm1
		
		;check collision with shot if not first_move and x > shipstartx		
		mov rax, qword [first_move]
		cmp rax, 1
		jne callchkcolshot
		ucomisd xmm0, [SHIPSTARTX]
		jb callchkcolshot 
		jmp cont17
		
callchkcolshot:	call checkcolshot
		
cont17:		push rsi
		push rcx
		movq rdi, xmm0
		movq rsi, xmm1
		mov rdx, [sortition_bombs]
		call checknewenemy
		pop rcx
		pop rsi

		call getnextgoal				
		
nextship:	inc rcx
		cmp rcx, 8
		jne loopmoveships
end3:		add rsp, 8
		ret	
;----------------------------------------------------
;procedure that generate a random number to decide if should create a new enemy, if so create it
;rdi - x coordinate
;rsi - y coordinate
;rdx - max integer to random
;affected: rax, rcx, rdx, rsi, r8
checknewenemy:	sub rsp, 8
		mov rax, [first_move]
		cmp rax, 1
		je endcne

		;divide by delta (push/pop xmm0 wich is shared with another subroutines of move_ships)
		sub rsp,16
		movdqu oword [rsp], xmm0
		cvtsi2sd xmm0, rdx
		divsd xmm0, [delta]
		cvtsd2si rdx, xmm0
		movdqu xmm0, oword [rsp]
		add rsp, 16

		mov r8, rsi
		xor rax, rax
		rdrand ax
		mov r9, rdx
		xor rdx, rdx
		mov rcx, r9
		div cx
		shr r9, 1
		cmp dx, r9w
		jne endcne		
		
		;find the first destroyed enemy
		xor rcx, rcx
lnewenemy:	mov rax, rcx
		mov rdx, enemy_size
		mul rdx
		add rax, enemies 
		mov rsi, rax
		mov rax, [rsi + enemy.destroyed]
		cmp rax, 1
		jne cont6
		mov rax, [tv]
		sub rax, [rsi + enemy.timedestroyed]
		cmp rax, 2
		jb cont6		
		
		;generate random enemy kind
		push rcx
		xor rax, rax
		rdrand ax
		
		mov rcx, [stage]
		cmp rcx, 4
		jbe stagebe4
		mov rcx, 4
		
stagebe4:	xor rdx, rdx
		div cx
		mov [rsi + enemy.kind], rdx
		pop rcx
				
		mov qword [rsi + enemy.destroyed], 0
		mov [rsi + enemy.x], rdi
		mov [rsi + enemy.xorigin], rdi
		mov [rsi + enemy.y], r8
		
		mov qword [playblip], 1
		
		jmp endcne
		
cont6:		inc rcx
		cmp rcx, [ENEMIES_COUNT]
		jne lnewenemy
		
endcne:		add rsp, 8
		ret
;----------------------------------------------------
create_enemies:	sub rsp, 8
		xor rcx, rcx
loopcrenemies:	mov rax, rcx
		mov rdx, enemy_size
		mul rdx
		add rax, enemies
		mov rsi, rax
		mov qword [rsi + enemy.kind], 0
		mov qword [rsi + enemy.destroyed], 1
		mov qword [rsi + enemy.timedestroyed], 0
		inc rcx
		cmp rcx, [ENEMIES_COUNT]
		jne loopcrenemies
		add rsp, 8
		ret
;----------------------------------------------------
create_ships:	sub rsp, 8
		mov r8, [SHIPDISTANCE]
		xor rcx, rcx
loopcreships:	mov rax, rcx
		mov rdx, ship_size
		mul rdx
		add rax, ships
		mov rsi, rax
		mov rax, rcx
		mul r8
				
		cvtsi2sd xmm0, rax
		movq xmm1, [SHIPSTARTX]
		addsd xmm0, xmm1
		movq rax, xmm0
		
		mov [rsi + ship.x], rax
		mov rax, [SHIPSTARTY]
		mov [rsi + ship.y], rax
		mov rdx, rcx
		dec rdx
		mov [rsi + ship.prior], rdx
		add rdx, 2
		mov [rsi + ship.next], rdx
		mov qword [rsi + ship.destroyed], 0
		movq xmm12, [DQ_ZERO]
		movq qword [rsi + ship.goalx], xmm12
		movq qword [rsi + ship.goaly], xmm12
		inc rcx
		cmp rcx, 8
		jne loopcreships
		mov rax, [START_GOALX]
		mov [ship_goalx], rax		
		mov rax, [START_GOALY]
		mov [ship_goaly], rax
		mov rax, [SHIPSTARTX]
		mov [ship_x], rax
		mov rax, [SHIPSTARTY]
		mov [ship_y], rax			
		mov qword [first_move], 1
		mov rax, [SORTITION_BOMBS_START]
		mov qword [sortition_bombs], rax
		
		mov rdi, qword [backgroundwavsource]
		call alSourceStop wrt ..plt		
		
		mov rdi, qword [openportalwavsource]
		call alSourcePlay wrt ..plt				
		
		add rsp, 8
		ret
;----------------------------------------------------
draw_hud:	sub rsp, 8
		
		mov byte [score_with_label_str], 0
		mov rdi, score_with_label_str
		mov rsi, SCORE_LABELSTR
		call strcat
		
		mov rdi, [score]
		mov rsi, score_str
		call inttostr
		
		mov rdi, score_strzero_str
		mov rsi, score_str
		mov rdx, 5
		call strzero

		mov rdi, score_with_label_str
		mov rsi, score_strzero_str
		call strcat

		lea rdi, [score_with_label_str]
		lea rsi, [glutBitmapHelvetica18]
		mov rdx, 0
		mov rcx, [HEADER_X]
		call draw_text		
		
		mov byte [stage_with_label_str], 0
		mov rdi, stage_with_label_str
		mov rsi, STAGE_LABELSTR
		call strcat
		
		mov rdi, [stage]
		mov rsi, stage_str
		call inttostr
		
		mov rdi, stage_strzero_str
		mov rsi, stage_str
		mov rdx, 3
		call strzero

		mov rdi, stage_with_label_str
		mov rsi, stage_strzero_str
		call strcat

		lea rdi, [stage_with_label_str]
		lea rsi, [glutBitmapHelvetica18]
		mov rdx, 150
		mov rcx, [HEADER_X]
		call draw_text		
		
		mov rdi, [high_score]
		mov rsi, high_score_str
		call inttostr

		mov rdi, high_score_strzero_str
		mov rsi, high_score_str
		mov rdx, 5
		call strzero
		
		mov byte [high_score_with_label_str + 12], 0
		mov rdi, high_score_with_label_str
		mov rsi, high_score_strzero_str
		call strcat
		
		lea rdi, [high_score_with_label_str]
		lea rsi, [glutBitmapHelvetica18]
		mov rdx, 280
		mov rcx, [HEADER_X]
		call draw_text		

		lea rdi, [RECORDIST_STR]
		lea rsi, [glutBitmapHelvetica18]
		mov rdx, 480
		mov rcx, [HEADER_X]
		call draw_text		

		mov rax, [fps_enabled]
		cmp rax, 0
		je notdrawfps
		mov rdi, [fps]
		lea rsi, [fpsstr]
		mov rdx, 1250
		mov rcx, [HEADER_X]
		mov r8, glutBitmapHelvetica18
		mov r9, FPS_LABELSTR
		call draw_text_pair

notdrawfps:	mov rax, [cannon_destroyed]
		cmp rax, 1
		jne enddraw_hud

		call glPushMatrix wrt ..plt

		mov rdi, [GL_DEPTH_TEST]
		call glDisable wrt ..plt
		
		mov rdi, [GL_LIGHTING]
		call glDisable wrt ..plt
		
		mov rdi, [GL_TEXTURE_2D]
		call glDisable wrt ..plt

		mov rdi, [GL_TEXTURE_ENV]
		mov rsi, [GL_TEXTURE_ENV_MODE]
		mov rdx, [GL_DECAL]
		call glTexEnvf wrt ..plt

		movd xmm0, dword [DD_XGAMEOVER]
		movd xmm1, dword [DD_YGAMEOVER]
		movd xmm2, dword [DD_ZERO]
		call glTranslatef wrt ..plt

		movd xmm0, dword [TEXT_SCALE]
		movd xmm1, dword [TEXT_SCALE]
		movd xmm2, dword [DD_ONE]
		call glScalef wrt ..plt
						
		lea rdi, [glutStrokeRoman]
		mov rsi, GAME_OVER_STRING
		call glutStrokeString wrt ..plt		

		call glPopMatrix wrt ..plt

		mov rdi, [GL_TEXTURE_2D]
		call glEnable wrt ..plt
				
enddraw_hud:	add rsp, 8		
		ret
;----------------------------------------------------	
;Subroutine from draw_enemies	
draw_explosion:	sub rsp, 8
		cvtsi2sd xmm4, qword [explosion1_width]
		divsd xmm4, [DQ_TWO]
		cvtsi2sd xmm5, qword [missile_width]
		divsd xmm5, [DQ_TWO]
		subsd xmm4, xmm5
		cvtsd2ss xmm4, xmm4
		
		mov rdi, [tv]
		sub rdi, [rsi + enemy.timedestroyed]
		cmp rdi, 1
		ja enddrwxplsn
		cvtsd2ss xmm0, qword [rsi + enemy.x]
		subss xmm0, xmm4
		cvtsd2ss xmm1, qword [rsi + enemy.y]
		cvtsi2ss xmm2, dword [explosion1_width]
		cvtsi2ss xmm3, dword [explosion1_height]
		mov rax, [tv + 8]
		mov rdi, [MICROSECONDS_BETWEEN_FLASHES]
		div rdi
		cmp rdx, [HALF_MICROSECONDS_BETWEEN_FLASHES]		
		ja explosion3
		mov rdi, [explosion1_texnum]
		jmp drawexpl2
explosion3:	mov rdi, [explosion2_texnum]
drawexpl2:	mov rbx, rcx
		call drawspr		
		mov rcx, rbx
enddrwxplsn:	add rsp, 8
		ret		
;----------------------------------------------------
;Subroutine from draw_greenbomb
draw_greenbomb:	sub rsp, 8

		mov rax, [tv + 8]
		xor rdx, rdx
		mov r9, MICROSECONDS_FRAME_GREENBOMB
		div r9
		mov r8, MICROSECONDS_FRAME_GREENBOMB
		shr r8, 1
		cmp rdx, r8
		ja greenbomb2
		
		mov rdi, [greenbomb_texnum]
		jmp drawgreenb
greenbomb2:	mov rdi, [greenbomb2_texnum]
drawgreenb:	cvtsi2ss xmm2, dword [greenbomb_width]
		cvtsi2ss xmm3, dword [greenbomb_height]
		mov rbx, rcx
		call drawspr
		add rsp, 8
		ret
;----------------------------------------------------
;Subroutine from draw
draw_enemies:	sub rsp, 8
		xor rcx, rcx
loop3:		mov rax, rcx
		mov rdx, enemy_size
		mul rdx
		add rax, enemies
		mov rsi, rax
		mov rax, qword [rsi + enemy.destroyed]
		cmp rax, 1
		je drawdestr2
		cvtsd2ss xmm0, qword [rsi + enemy.x]
		cvtsd2ss xmm1, qword [rsi + enemy.y]
		cmp qword [rsi + enemy.kind], 1
		je draw_redbomb
		cmp qword [rsi + enemy.kind], 2
		je draw_grnbmb
		cmp qword [rsi + enemy.kind], 3
		je draw_balloon
		cvtsi2ss xmm2, dword [missile_width]
		cvtsi2ss xmm3, dword [missile_height]
		mov rdi, [missile_texnum]
		mov rbx, rcx
		call drawspr
		jmp cont10
		
draw_grnbmb:	call draw_greenbomb
		
		jmp cont10
draw_redbomb:	mov rax, [tv + 8]
		xor rdx, rdx
		mov r9, MICROSECONDS_FLASH_REDBOMB
		div r9
		mov r8, MICROSECONDS_FLASH_REDBOMB
		shr r8, 1
		cmp rdx, r8
		ja redbomb2
		mov rdi, [redbomb_texnum]
		jmp drawredbomb		
redbomb2:	mov rdi, [redbomb2_texnum]
drawredbomb:	cvtsi2ss xmm2, dword [redbomb_width]
		cvtsi2ss xmm3, dword [redbomb_height]
		mov rbx, rcx
		call drawspr
		jmp cont10
		
draw_balloon:	mov rax, [tv + 8]
		xor rdx, rdx
		mov r9, MICROSECONDS_FLASH_BALLOON
		div r9
		mov r8, MICROSECONDS_FLASH_BALLOON
		shr r8, 1
		cmp rdx, r8
		ja balloon2
		mov rdi, [balloon_texnum]
		jmp drawballoon		
balloon2:	mov rdi, [balloon2_texnum]
drawballoon:	cvtsi2ss xmm2, dword [balloon_width]
		cvtsi2ss xmm3, dword [balloon_height]
		mov rbx, rcx
		call drawspr		
		
cont10:		mov rcx, rbx
		jmp next4
			
drawdestr2:	call draw_explosion
		
next4:		inc rcx
		cmp rcx, [ENEMIES_COUNT]
		jne loop3
		add rsp, 8
		ret		
;----------------------------------------------------
draw_opening3:	sub rsp, 8

		call clrscr

		call glPushMatrix wrt ..plt

		movd xmm0, dword [TREVASKASII_X]
		movd xmm1, dword [TREVASKASII_Y]
		cvtsi2ss xmm2, dword [trevaskasii_width]
		cvtsi2ss xmm3, dword [trevaskasii_height]
		mov rdi, [trevaskasii_texnum]
		call drawspr
		
		movd xmm0, dword [trevaskas_x]
		movd xmm1, dword [trevaskas_y]	
		cvtsi2ss xmm2, dword [trevaskas_width]
		cvtsi2ss xmm3, dword [trevaskas_height]
		mov rdi, [trevaskas_texnum]
		call drawspr	
		
		mov rdi, PRESS_ANY_KEY_STR
		lea rsi, [glutBitmapHelvetica18]
		mov rdx, PRESS_ANY_KEY_X
		mov rcx, PRESS_ANY_KEY_Y
		call draw_text						

		call glPopMatrix wrt ..plt

		add rsp, 8
		ret
;----------------------------------------------------
draw_opening2:	sub rsp, 8

		call clrscr

		call glPushMatrix wrt ..plt

		movd xmm0, dword [AGAMEBY_X]
		movd xmm1, dword [AGAMEBY_Y]
		cvtsi2ss xmm2, dword [agameby_width]
		cvtsi2ss xmm3, dword [agameby_height]
		mov rdi, [agameby_texnum]
		call drawspr
		
		call glPopMatrix wrt ..plt

		add rsp, 8
		ret
;----------------------------------------------------
draw_opening1:	sub rsp, 8

		call clrscr

		call glPushMatrix wrt ..plt
		
		movd xmm0, dword [TEZON_X]
		movd xmm1, dword [tezon_y]
		cvtsi2ss xmm2, dword [tezon_width]
		cvtsi2ss xmm3, dword [tezon_height]
		mov rdi, [tezon_texnum]
		call drawspr

		movd xmm0, dword [THIRTY_YEARS_X]
		movd xmm1, dword [tezon_y]
		addss xmm1, [THIRTY_YEARS_Y_SHIFT]
		cvtsi2ss xmm2, dword [thirty_years_width]
		cvtsi2ss xmm3, dword [thirty_years_height]
		mov rdi, [thirty_years_texnum]
		call drawspr
		
		call glPopMatrix wrt ..plt

		add rsp, 8
		ret
;----------------------------------------------------
draw_portal:	sub rsp, 8
		mov rax, [first_move]
		cmp rax, 1
		jne notdrawportal
		
		call glPushMatrix wrt ..plt
				
		movd xmm0, dword [portal_rotation]
		cvtsd2ss xmm1, [delta]
		mulss xmm1, [PORTAL_ROTATION_SPEED]
		addss xmm0, xmm1		
		movd dword [portal_rotation], xmm0
		
		movd xmm0, dword [portal_x]
		movd xmm1, dword [portal_y]	
		cvtsi2ss xmm2, dword [portal_width]
		cvtsi2ss xmm3, dword [portal_height]
		movd xmm4, dword [portal_rotation]
		mov rdi, [portal_texnum]
		call drawrotatedspr	
		
		call glPopMatrix wrt ..plt
		
notdrawportal:	add rsp, 8
		ret
;----------------------------------------------------
draw_ships:	sub rsp, 8
		xor rcx, rcx
loop1:		mov rax, rcx
		mov rdx, ship_size
		mul rdx
		add rax, ships	
		mov rbx, rcx
		
		;if is the first move, do not show ships that have x > portal_center_x		
		mov rdx, qword [first_move]
		cmp rdx, 1
		jne cont16
		movq xmm0, qword [rax + ship.x]
		ucomisd xmm0, [PORTAL_CENTER_X]
		jb cont16
		jmp end2
		
cont16:         mov rsi, qword [rax + ship.destroyed]
                cmp rsi, 1
                je drawdestr
		cvtsd2ss xmm0, qword [rax + ship.x]
		cvtsd2ss xmm1, qword [rax + ship.y]
		cvtsi2ss xmm2, dword [ship_width]
		cvtsi2ss xmm3, dword [ship_height]
		mov rdi, [ship_texnum]
		call drawspr
		jmp end2
		
drawdestr:	mov rdi, [tv]
		sub rdi, [rax + ship.timedestroyed]
		cmp rdi, 1
		ja end2
		cvtsd2ss xmm0, qword [rax + ship.x]
		cvtsd2ss xmm1, qword [rax + ship.y]
		cvtsi2ss xmm2, dword [explosion1_width]
		cvtsi2ss xmm3, dword [explosion1_height]
		mov rax, [tv + 8]
		mov rdi, [MICROSECONDS_BETWEEN_FLASHES]
		div rdi
		cmp rdx, [HALF_MICROSECONDS_BETWEEN_FLASHES]
		ja explosion2
		mov rdi, [explosion1_texnum]
		jmp drawexpl
explosion2:	mov rdi, [explosion2_texnum]
drawexpl:	call drawspr
		
end2:		mov rcx, rbx			
		inc rcx
		cmp rcx, 8
		jne loop1
		add rsp, 8
		ret		
;----------------------------------------------------
initshipgoal:	sub rsp, 8
		xor rax, rax
		rdrand ax
		xor rdx, rdx
		mov cx, 1334
		div cx
		cvtsi2sd xmm0, edx
		movq qword [ship_goalx], xmm0
	
		xor rax, rax
		rdrand ax
		xor rdx, rdx
		mov cx, 734
		div cx
		cvtsi2sd xmm0, edx
		movq qword [ship_goaly], xmm0	
		
		add rsp, 8
		ret
;----------------------------------------------------
loadaudio:	sub rsp, 8
		mov rdi, SHOTWAVFILE
		call alutCreateBufferFromFile
		mov [shotwavbuffer], rax
            
		mov rdi, 1
		mov rsi, shotwavsource
		call alGenSources wrt ..plt

		mov rdi, [shotwavsource]
		mov rsi, [AL_BUFFER]
		mov rdx, [shotwavbuffer]
		call alSourcei wrt ..plt
            
		mov rdi, EXPLOSIONWAVFILE
		call alutCreateBufferFromFile
		mov [explosionbuffer], rax
            
		mov rdi, 1
		mov rsi, explosionsource
		call alGenSources wrt ..plt

		mov rdi, [explosionsource]
		mov rsi, [AL_BUFFER]
		mov rdx, [explosionbuffer]
		call alSourcei wrt ..plt

		mov rdi, OPENPORTALWAVFILE
		call alutCreateBufferFromFile
		mov [openportalwavbuffer], rax
            
		mov rdi, 1
		mov rsi, openportalwavsource
		call alGenSources wrt ..plt

		mov rdi, [openportalwavsource]
		mov rsi, [AL_BUFFER]
		mov rdx, [openportalwavbuffer]
		call alSourcei wrt ..plt

		mov rdi, BACKGROUNDWAVFILE
		call alutCreateBufferFromFile
		mov [backgroundwavbuffer], rax
            
		mov rdi, 1
		mov rsi, backgroundwavsource
		call alGenSources wrt ..plt

		mov rdi, [backgroundwavsource]
		mov rsi, [AL_BUFFER]
		mov rdx, [backgroundwavbuffer]
		call alSourcei wrt ..plt

		mov rdi, [backgroundwavsource]
		mov rsi, [AL_LOOPING]
		mov rdx, 1
		call alSourcei wrt ..plt
		
		mov rdi, BLIPWAVFILE
		call alutCreateBufferFromFile
		mov [blipwavbuffer], rax
            
		mov rdi, 1
		mov rsi, blipwavsource
		call alGenSources wrt ..plt

		mov rdi, [blipwavsource]
		mov rsi, [AL_BUFFER]
		mov rdx, [blipwavbuffer]
		call alSourcei wrt ..plt

		mov rdi, CANNONEXPLOSIONWAVFILE
		call alutCreateBufferFromFile
		mov [cannonexplosionwavbuffer], rax
            
		mov rdi, 1
		mov rsi, cannonexplosionwavsource
		call alGenSources wrt ..plt

		mov rdi, [cannonexplosionwavsource]
		mov rsi, [AL_BUFFER]
		mov rdx, [cannonexplosionwavbuffer]
		call alSourcei wrt ..plt
		
		add rsp, 8
		ret
;----------------------------------------------------
initgl: 	sub rsp,8

		call glutInit wrt ..plt

		mov rdi, [GLUT_RGBA]
		call glutInitDisplayMode wrt ..plt

		cmp qword [fullscreen], 0
		jne gofullscreen
		
		;Next three commented calls to glut start windowed mode
		mov rdi, [DQ_I_SCREENWIDTH]
		mov rsi, [DQ_I_SCREENHEIGHT]
		call glutInitWindowSize wrt ..plt

		xor rdi, rdi
		xor rsi, rsi
		call glutInitWindowPosition wrt ..plt

		mov rdi, WINTITLE
		call glutCreateWindow wrt ..plt
		mov [WINHANDLE], rax
		
		jmp skipfullscreen

gofullscreen:	mov rdi, GAME_MODE_STRING
		call glutGameModeString wrt ..plt
		
		call glutEnterGameMode wrt ..plt

		mov rdi, GLUT_CURSOR_NONE
		call glutSetCursor wrt ..plt

skipfullscreen:	mov rdi, draw
		call glutDisplayFunc wrt ..plt

		mov rdi, GLUT_IGNORE_KEY_REPEAT
		call glutIgnoreKeyRepeat wrt ..plt

		mov rdi, dospeckey
		call glutSpecialFunc wrt ..plt

		mov rdi, doupspeckey
		call glutSpecialUpFunc wrt ..plt

		mov rdi, onidle
		call glutIdleFunc wrt ..plt

		mov rdi, [GL_TEXTURE_2D]
		call glEnable wrt ..plt
        
		mov rdi, [GL_SMOOTH]
		call glShadeModel wrt ..plt

		mov rdi, [GL_DEPTH_TEST]
		call glEnable wrt ..plt

		mov rdi, [GL_EQUAL]
		call glDepthFunc wrt ..plt

		mov rdi, [GL_PERSPECTIVE_CORRECTION_HINT]
		mov rsi, [GL_NICEST]
		call glHint wrt ..plt

		mov rdi, [GL_SRC_ALPHA]
		mov rsi, [GL_ONE_MINUS_SRC_ALPHA]
		call glBlendFunc wrt ..plt
            
        	mov rdi, dokey
        	call glutKeyboardFunc wrt ..plt
        
        	mov rdi, doupkey
        	call glutKeyboardUpFunc wrt ..plt    
            
		mov rdi, [GL_BLEND]
		call glEnable wrt ..plt

		mov rdi, [GLUT_ACTION_ON_WINDOW_CLOSE]
		mov rsi, [GLUT_ACTION_GLUTMAINLOOP_RETURNS]
		call glutSetOption wrt ..plt

		add rsp, 8

		ret
;----------------------------------------------------	
loadtextures: 	sub rsp, 8
		mov rdi, BACKGROUND_PATH
		call loadtex
		mov [background_texturenum], rax
		mov [background_width], rdi
		mov [background_height], rsi
	
		mov rdi, CANNON_PATH
		call loadtex
		mov [cannon_texnum], rax
		mov [cannon_width], rdi
		mov [cannon_height], rsi
	
		mov rdi, SHOT_PATH
		call loadtex
		mov [shot_texnum], rax
		mov [shot_width], rdi
		mov [shot_height], rsi
	
		mov rdi, SHIP_PATH
		call loadtex
		mov [ship_texnum], rax
		mov [ship_width], rdi
		mov [ship_height], rsi

		mov rdi, EXPLOSION1_PATH
		call loadtex
		mov [explosion1_texnum], rax
		mov [explosion1_width], rdi
		mov [explosion1_height], rsi

		mov rdi, EXPLOSION2_PATH
		call loadtex
		mov [explosion2_texnum], rax
		mov [explosion2_width], rdi
		mov [explosion2_height], rsi

		mov rdi, MISSILE_PATH
		call loadtex
		mov [missile_texnum], rax
		mov [missile_width], rdi
		mov [missile_height], rsi

		mov rdi, REDBOMB_PATH
		call loadtex
		mov [redbomb_texnum], rax
		mov [redbomb_width], rdi
		mov [redbomb_height], rsi
		
		mov rdi, REDBOMB2_PATH
		call loadtex
		mov [redbomb2_texnum], rax
		mov [redbomb2_width], rdi
		mov [redbomb2_height], rsi

		mov rdi, BALLOON_PATH
		call loadtex
		mov [balloon_texnum], rax
		mov [balloon_width], rdi
		mov [balloon_height], rsi

		mov rdi, BALLOON2_PATH
		call loadtex
		mov [balloon2_texnum], rax
		mov [balloon2_width], rdi
		mov [balloon2_height], rsi

		mov rdi, GREENBOMB_PATH
		call loadtex
		mov [greenbomb_texnum], rax
		mov [greenbomb_width], rdi
		mov [greenbomb_height], rsi

		mov rdi, GREENBOMB2_PATH
		call loadtex
		mov [greenbomb2_texnum], rax
		mov [greenbomb2_width], rdi
		mov [greenbomb2_height], rsi
	
		mov rdi, TREVASKAS_PATH
		call loadtex
		mov [trevaskas_texnum], rax
		mov [trevaskas_width], rdi
		mov [trevaskas_height], rsi
	
		mov rdi, PORTAL_PATH
		call loadtex
		mov [portal_texnum], rax
		mov [portal_width], rdi
		mov [portal_height], rsi
		
		mov rdi, TEZON_PATH
		call loadtex
		mov [tezon_texnum], rax
		mov [tezon_width], rdi
		mov [tezon_height], rsi

		mov rdi, THIRTY_YEARS_PATH
		call loadtex
		mov [thirty_years_texnum], rax
		mov [thirty_years_width], rdi
		mov [thirty_years_height], rsi

		mov rdi, TREVASKASII_PATH
		call loadtex
		mov [trevaskasii_texnum], rax
		mov [trevaskasii_width], rdi
		mov [trevaskasii_height], rsi

		mov rdi, AGAMEBY_PATH
		call loadtex
		mov [agameby_texnum], rax
		mov [agameby_width], rdi
		mov [agameby_height], rsi
	
		add rsp, 8
		ret
;----------------------------------------------------		
inittime:	sub rsp, 8
		mov rax, 0
		mov [tv], rax
		mov [tv + 4], rax
		mov [tz], rax
		mov [tz + 4], rax
		mov rax, [SYS_GETTIMEOFDAY]
		mov rdi, tv
		mov rsi, tz
		syscall
		mov rax, [tv]
		mov [begintime], rax	
		mov [last_second], rax
		mov [first_tv], rax		
		
		xor rax, rax
		mov [tv_last_shot], rax
		mov [tv_last_shot + 8], rax
		mov [tz_last_shot], rax
		mov [tz_last_shot + 8], rax
		mov rax, [SYS_GETTIMEOFDAY]
		mov rdi, tv_last_shot
		mov rsi, tz_last_shot
		syscall		
			
		add rsp, 8
		ret
;----------------------------------------------------
openalcleanup:	sub rsp, 8
		mov rdi, 1
		mov rsi, shotwavsource            
		call alDeleteSources wrt ..plt
            
		mov rdi, 1
		mov rsi, shotwavbuffer
		call alDeleteBuffers wrt ..plt

		call alutExit wrt ..plt

		add rsp, 8
		ret
;----------------------------------------------------
print_help:	sub rsp, 8

		mov rdi, POSSIBLE_ARGUMENTS_STR
		call print
		
		mov rdi, NEW_LINE_STR
		call print
		
		mov rdi, ARGUMENT_WINDOWED_STR
		call print
		
		mov rdi, ARGUMENT_FPS_STR
		call print
		
		mov rdi, ARGUMENT_FPS_DELIMITER_STR
		call print
		
		mov rdi, ARGUMENT_HELP_STR
		call print

		add rsp, 8
		ret
;----------------------------------------------------
inv_option:	sub rsp, 8

		mov rdi, INVALID_ARG_STR
		call print
		
		mov rdi, TRY_HELP_STR
		call print

		add rsp, 8
		ret
;----------------------------------------------------
proc_arguments:	;sub rsp, 8
		push r12
		push r13
		push r14
		mov r12, rsi
		mov r14, [rdi]
		
		cmp r14, 2
		jl endprcarg
		

		mov r13, 1
loopprcarg:	mov rdi, [r12 + r13 * 8]
		mov rsi, WINDOWED_STR
		call mystrcmp
		cmp rax, 1
		jne checkfps
		mov qword [fullscreen], 0
		jmp nextarg
		
checkfps:	mov rdi, [r12 + r13 * 8]
		mov rsi, FPS_STR
		call mystrcmp
		cmp rax, 1
		jne checkfpsdeli
		mov qword [fps_enabled], 1
		jmp nextarg

checkfpsdeli:	mov rdi, [r12 + r13 * 8]
		mov rsi, FPS_DELIMITER_STR
		call mystrcmp
		cmp rax, 1
		jne checkhelp
		mov qword [fps_delimiter], 1
		jmp nextarg
		
checkhelp:	mov rdi, [r12 + r13 * 8]
		mov rsi, HELP_STR
		call mystrcmp
		cmp rax, 1
		jne invoption
		call print_help
		mov rax, 60; sys_exit
		mov rdi, 0
		syscall
		
invoption:	call inv_option
		mov rax, 60; sys_exit
		mov rdi, 0
		syscall
		
nextarg:	inc r13
		cmp r13, r14
		jl loopprcarg

endprcarg:	pop r14
		pop r13
		pop r12
		;add rsp, 8
		ret
;----------------------------------------------------
main:		mov rbp, rsp; for correct debugging

		sub rsp, 8
		mov [rsp], edi
		lea rdi, [rsp]

		;Initialize abs mask
		mov rax, [ABS_MASK_QWORD]
		mov qword [ABS_MASK_OWORD], rax 
		mov qword [ABS_MASK_OWORD + 8], rax			

		push rdi
		push rsi

		call proc_arguments
		call initshipgoal

		call alutInit wrt ..plt
	
		call loadaudio
	
		pop rsi
		pop rdi
		call initgl

		call loadtextures		
		call inittime
              
		mov rax, SYS_open
		mov rsi, O_RDONLY
		or rsi, O_NONBLOCK
		mov rdi, JOYSTICK_PATH
		syscall
		mov qword [joystickdescriptor], rax     
		
		call setnewgame				
              
		call glutMainLoop wrt ..plt
		
		mov rax, SYS_close
		mov rdi, qword [joystickdescriptor]
		syscall            		

		call openalcleanup

		mov rax, SYS_write
		mov rdi, 1
		mov rsi, TREVASKASII_COPYRIGHT_MESSAGE
		mov rdx, TREVASKASII_COPYRIGHT_MESSAGE_LEN
		syscall

		mov rax, SYS_write
		mov rdi, 1
		mov rsi, VERSION_MESSAGE
		mov rdx, VERSION_MESSAGE_LEN
		syscall

		mov rax, SYS_EXIT_GROUP
		mov rdi, 0
		syscall
